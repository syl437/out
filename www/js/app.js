// Ionic Starter App
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic','openfb', 'starter.controllers','ngStorage', 'ngCordova','ngSanitize'])
.run(function($rootScope,$ionicPlatform,$http,OpenFB) 
{ 
	

	//$rootScope.userid = 1;
	$rootScope.isConnected = 0;
	//console.log('user id hard coded to 1')
	$rootScope.Host = 'http://tapper.co.il/outdoor/php/';
	$rootScope.jsonUrl = $rootScope.Host+'/get_Esek.php';
	$rootScope.linesUrl = $rootScope.Host+'/get_Line.php';
	$rootScope.GeneralUrl = $rootScope.Host+'/get_general.php';
	$rootScope.CrewUrl = $rootScope.Host+'/get_crew.php';
	$rootScope.QuestionsUrl = $rootScope.Host+'/get_questions.php';
	$rootScope.EsekAdsUrl = $rootScope.Host+'/getEsekAds.php';
	$rootScope.GalleryInfo = '';
	$rootScope.SelectedImage  = '';
	
	OpenFB.init('365225076964472');
	
	//$rootScope.jsonUrl = 'http://tapper.co.il/soroka/php/getStories.php'
	$http.get($rootScope.linesUrl).success(function(data)
	{
		$rootScope.Lines = data;
		//console.log("Lines")
		//console.log($rootScope.Lines)
	});
	
	$http.get($rootScope.jsonUrl).success(function(data)
	{
		$rootScope.Esek = data;
		//console.log("Esek")
		//console.log($rootScope.Esek)
	});
	
	
	$rootScope.jsonUrl = 'js/json/movies.json';
	$http.get($rootScope.jsonUrl).success(function(data)
	{
		$rootScope.moviesArray = data;
	})
	
	
	/*
	// get user data
		$http.get($rootScope.Host+'/Getusers.php?id='+$rootScope.userid).success(function(data)
	{
		$rootScope.userArray = data.response;
	})
	
	//get user messages
		$http.get($rootScope.Host+'/get_user_messages.php?user='+$rootScope.userid).success(function(data)
	{
		$rootScope.messageCount = data.response.length; 
		$rootScope.userMessagesArray = data.response;
	})
*/

	//get esek ads
		$http.get($rootScope.EsekAdsUrl).success(function(data)
	{
		$rootScope.EsekAds = data.response;
		//alert ($rootScope.EsekAds[0].image)
	})	
	

	//get general data
		$http.get($rootScope.GeneralUrl).success(function(data)
	{
		$rootScope.GeneralData = data;
	})	

	//get crew data
		$http.get($rootScope.CrewUrl).success(function(data)
	{
		$rootScope.CrewData = data.response;
	})	

	//get questions & answers
		$http.get($rootScope.QuestionsUrl).success(function(data)
	{
		$rootScope.QuestionsData = data.response;
	})	
	
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
	  //window.plugins.insomnia.keepAwake();
	 if (typeof(window.plugins) !== 'undefined') {
       window.plugins.insomnia.keepAwake();
     }


    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
	
	$rootScope.Category = 1;
//	$rootScope.Host = 'http://localhost/outdoor/php/';
	
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider


    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  
  .state('app.connect', {
    url: '/connect',
    views: {
      'menuContent': {
        templateUrl: 'templates/connect.html',
        controller: 'ConnectController'
      }
    }
  })
  
   .state('app.main', {
    url: '/main',
    views: {
      'menuContent': {
        templateUrl: 'templates/main.html',
        controller: 'MainController'
      }
    }
  })
  
   .state('app.List', {
    url: '/List/:CategoryId',
    views: {
      'menuContent': {
        templateUrl: 'templates/list.html',
        controller: 'ListPage'
      }
    }
  })
 
   .state('app.favorites', {
    url: '/favorites/',
    views: {
      'menuContent': {
        templateUrl: 'templates/favorites.html',
        controller: 'FavoritesPage'
      }
    }
  })

   .state('app.nearby', {
    url: '/nearby',
    views: {
      'menuContent': {
        templateUrl: 'templates/nearby.html',
        controller: 'NearByCtrl'
      }
    }
  })
  
  .state('app.categories', {
    url: '/categories',
    views: {
      'menuContent': {
        templateUrl: 'templates/menu_page.html',
        controller: 'MenuController'
      }
    }
  })
  
   .state('app.line', {
    url: '/Line/:itemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/line.html',
        controller: 'LinePage'
      }
    },
	resolve: {
      item: function() {
        return 2
      }
    }
  })
  
  .state('app.pub', {
    url: '/Pub/:itemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/pub.html',
        controller: 'LinePage'
      }
    },
	resolve: {
      item: function() {
        return 1
      }
    }
  })
  
  
  .state('app.gallery', {
    url: '/Gallery/:itemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/gallery.html',
        controller: 'GalleryController'
      }
    }
  })
  
    .state('app.coupons', {
    url: '/coupons/:itemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/coupons.html',
        controller: 'CouponsController'
      }
    }
  })
  
    .state('app.profile', {
    url: '/profile/:itemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/profile.html',
        controller: 'Profile'
      }
    }
  })
  
  
      .state('app.maincoupons', {
    url: '/maincoupons/:itemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/coupons_general.html',
        controller: 'CouponsController'
      }
    }
  })
  
  
  
    .state('app.news', {
    url: '/news/:itemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/news.html',
        controller: 'News'
      }
    }
  })
  
    .state('app.newspage', {
    url: '/newspage/:itemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/news_page.html',
        controller: 'News_page'
      }
    }
  })
  
      .state('app.review', {
    url: '/review/:itemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/review.html',
        controller: 'ReviewController'
      }
    }
  })
  
  .state('app.rate', {
    url: '/rate/:itemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/rate.html',
        controller: 'RateController'
      }
    }
  })
  
    .state('app.messages', {
    url: '/messages/:itemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/messages.html',
        controller: 'MessagesController'
      }
    }
  })
 
    .state('app.readmessage', {
    url: '/readmessage/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/read_message.html',
        controller: 'ReadMessageController'
      }
    }
  })
  
  
  .state('app.points', {
    url: '/points/:itemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/points.html',
        controller: 'Points'
      }
    }
  })
  
    .state('app.restmenu', {
    url: '/restmenu/:itemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/rest_menu.html',
        controller: 'restMenu'
      }
    }
  })
 
   .state('app.contact', {
    url: '/contact',
    views: {
      'menuContent': {
        templateUrl: 'templates/contact.html',
        controller: 'ContactCtrl'
      }
    }
  })

   .state('app.general', {
    url: '/general',
    views: {
      'menuContent': {
        templateUrl: 'templates/general.html',
        controller: 'GeneralCtrl'
      }
    }
  })  
  
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/main');
});


