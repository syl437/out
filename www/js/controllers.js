angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout,$localStorage) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  
  $scope.$on('$ionicView.enter', function(e) {
 
	$scope.showMenu = false;

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
  
  if ($localStorage.userid)
  {
	  $scope.showMenu = true;
  }
  
  $scope.userid = $localStorage.userid;
  $scope.name = $localStorage.name;
  $scope.profileimage = $localStorage.image;

 
  

});
})

.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];
})

.controller('PlaylistCtrl', function($scope, $stateParams) {
})

.controller('ConnectController', function($scope,$http,$ionicScrollDelegate,$ionicPlatform,$ionicPopup,OpenFB) 
{
	/*
	alert("01")
	OpenFB.getLoginStatus(function(response) 
	{
		alert("02")
		  if (response.status === 'connected') 
		  {
			$scope.uid = response.authResponse.userID;
			$scope.accessToken = response.authResponse.accessToken;
			alert($scope.uid + " : " + $scope.accessToken + " : " + response.status)
		  } 
		  else if (response.status === 'not_authorized') 
		  {
			// the user is logged in to Facebook, 
			// but has not authenticated your app
		  } 
		  else 
		  {
			// the user isn't logged in to Facebook.
		  }
 	});*/
	
	//public_profile,email,user_birthday
	 $scope.facebookLogin = function () 
	 {
		OpenFB.login('name,email,gender,user_birthday').then(function () 
		{
			$scope.getId();
		},
        function () 
		{
            alert('OpenFB login failed');
        });
     };
	 
	 $scope.getId = function ()
	 {
		  OpenFB.get('/me').success(function (user) 
		  {
			    console.log(user);
            	$scope.user = user;
				alert(user.name + " : " + user.id)
          });
	 }
})


.controller('MainController', function($scope,$http,$ionicScrollDelegate,$ionicPlatform,$ionicPopup,$localStorage,$state) 
{

	
	$scope.openInExternalBrowser = function()
	{
	 	window.open('http://tapper.co.il','_blank','vw=568,vh=768,vx=200,vy=0,buttoncolorbg=#BA8C3C'); 
	};
 
    $scope.shareViaTwitter = function(message, image, link) {
        $cordovaSocialSharing.canShareVia("twitter", message, image, link).then(function(result) {
            $cordovaSocialSharing.shareViaTwitter(message, image, link);
        }, function(error) {
            alert("Cannot share on Twitter");
        });
    }
		//if ($state.current.views.menuContent.controller == "MainController")
		//{
			//$ionicPlatform.registerBackButtonAction(registerBackButtonAction, 501);
			//$scope.registerBackButtonAction();

			
		//}			
	
		//$scope.registerBackButtonAction = function(e,code)
		
		/*
			function registerBackButtonAction(e,code) 
		{
			console.warn (code);

				if (!!e && typeof e.preventDefault === 'function') {
					e.preventDefault();
				}

				var confirmPopup = $ionicPopup.confirm({
					 title: '?האם ברצונך לצאת מהאפליקציה',
				   });
				   confirmPopup.then(function(res) {
					 if(res) {
					   navigator.app.exitApp();
					 } else {
					   e.preventDefault();
					 }
				   });		
		
				return false;
			}
		*/
		
    $http.get('js/json/pubs.json').success(function(data)
	{
		$scope.navTitle='<img class="title-image" src="img/main/ylogo.png" />'
		$scope.rightButton = '<img class="right-button" src="img/main/question.png" />'
		
		LevelOpen = 2;
		$scope.ginun = data;
		Page_Id = 1;
		
		$scope.navigateUrl = function (Path,Num) 
		{
			window.location.href = Path+String(Num);
			window.plugins.insomnia.keepAwake();
		}
		
		$scope.showInfo = function () 
		{
			angular.element('#InfoPopUp').css('visibility','visible');
		}
		
		$scope.hideInfo = function () 
		{
			angular.element('#InfoPopUp').css('visibility','hidden');
		}

	
		/*
		$ionicPlatform.registerBackButtonAction(function (event) 
		{
		var confirmPopup = $ionicPopup.confirm({
			 title: '?האם ברצונך לצאת מהאפליקציה',
		   });
		   confirmPopup.then(function(res) {
			 if(res) {
			   navigator.app.exitApp();
			 } else {
			   event.preventDefault();
			 }
		   });		
				
			});
			
		*/
			
		}
		,100);
		
	
		
})



.controller('MenuController', function($scope,$http) 
{
	$scope.navTitle='<img class="title-image" src="img/main/ylogo.png" />'
	
	
    $http.get('js/json/pubs.json').success(function(data)
	{
		
		Page_Id = 2;
		
		
		$scope.navigateUrl = function (Path,Num) 
		{
			//console.log("77 : " + Path+String(Num) )
			setTimeout(function(){ window.location.href = Path+String(Num); }, 0);	
		}
		
	});
})



.controller('ListPage', function($scope,$http,$stateParams,$rootScope,$ionicScrollDelegate,$localStorage) 
{
	//$scope.$on('$ionicView.enter', function(e) 
  	//{
		//setTimeout(function(){
			$scope.Days = Array();
			$scope.ShowDaysPanel = false;
			$scope.CurrentDayText = "";
			$scope.CurrentDay = "";
			$scope.imagePath = $rootScope.Host;
			$scope.EsekAds = $rootScope.EsekAds;
			//alert ($scope.EsekAds[0].image)
			$scope.navTitle='<img class="title-image" src="img/main/ylogo.png" />'
			$rootScope.Category = $stateParams.CategoryId;
			$scope.CatId = $stateParams.CategoryId;
			$scope.Category = parseInt($rootScope.Category);
			$scope.popButton = "img/menu/popu.png"
			$scope.LenButton = "img/menu/len.png"
		
			if($scope.Category == 4)
			{
				//$scope.infoArray = $rootScope.moviesArray;
				$scope.infoArray = $rootScope.Lines;
				$scope.ShowDaysPanel = true;
			}
			else if($scope.Category == 3 || $scope.Category == 2 || $scope.Category == 5)
			{
				$scope.infoArray = $rootScope.Lines;
				$scope.ShowDaysPanel = true;
			}
			else if($scope.Category == 1 || $scope.Category == 6)
			{
				$scope.infoArray = $rootScope.Esek;
				$scope.ShowDaysPanel = false;
			}
			
			//console.log("InfoArray")
			//console.log($scope.infoArray)
		
				
			$scope.Rand = Math.floor(Math.random()*2)+1;
			$scope.Rand1 = Math.floor(Math.random()*7)+1;
			$scope.Rand2 = Math.floor(Math.random()*7)+1;
			
			if($scope.Category == 1  || $scope.Category == 6)
			$scope.NextPage = "#/app/Pub/"
			else
			$scope.NextPage = "#/app/Line/"
			
			$scope.byCatIndex = function() 
			{
				return function(user) 
				{
					if($scope.CatId != 3 && $scope.CatId != 2 && $scope.CatId != 5 && $scope.CatId != 4 )
					{
						return user.CatIndex == $scope.CatId;
					}
					else
					{
						if(user.LineDate == $scope.CurrentDay)
						console.log("Dateee LineDate: " + user.LineDate + " : " + $scope.CurrentDay + " : " + user.articleId + " : "  + $scope.CatId + " : " + user.BusinessName);
						
						return user.articleId == $scope.CatId && String(user.LineDate) == String($scope.CurrentDay);
					}	
				}
			}
			$scope.PopLenClicked = function(nm)
			{
				if(nm ==1)
				{
					$scope.popButton = "img/menu/popc.png"
					$scope.LenButton = "img/menu/len.png"
				}
				else
				{
					$scope.popButton = "img/menu/popu.png"
					$scope.LenButton = "img/menu/lenc.png"
				}
			}
			
			$scope.navigateUrl = function (Path,Num) 
			{
				window.location.href = Path+String(Num);
			}
			
			$scope.shuffle_table = function () 
			{
				$scope.infoArray = shuffle($scope.infoArray);
			}
			
			$scope.setDays = function ()
			{
				$scope.Days = new Array();
				
				for(i=0;i<14;i++)
				{
					$scope.Obj = new Object();
					$scope.hours = (i*24)+24;
					$scope.Day = new Date(new Date().getTime() + $scope.hours * 60 * 60 * 1000);
					$scope.DD = $scope.gatDayName($scope.Day.getDay());
					$scope.Day = $scope.Day.toISOString().substring(0, 10);
					$scope.splitDay = $scope.Day.split('-');
					$scope.Day = $scope.splitDay[2]+ '-' + $scope.splitDay[1] + '-' + $scope.splitDay[0];
					$scope.Obj.date = $scope.Day;
					$scope.Obj.day = $scope.DD;
					$scope.Days.push($scope.Obj)
				}
				
				$scope.CurrentDayText = "יום " + $scope.Days[0].day + " " + $scope.Days[0].date;
				$scope.CurrentDay = $scope.Days[0].date;
			
			
				$scope.Days = $scope.Days.reverse();
				setTimeout(function() {
				 	$ionicScrollDelegate.$getByHandle('daysScroll').scrollTo(700,0,true);
					},200);
			}
			
			
			
			$scope.setCurrentDay = function(day) 
			{
				$scope.CurrentDayText = "יום " + $scope.Days[day].day + " " + $scope.Days[day].date;
				$scope.CurrentDay = $scope.Days[day].date;  
			/*
			$scope.LineDate = function()
			{
				return function(user) 
				{
					if (user.LineDate == $scope.Days[day].date)
					{
						console.log(3242434324)
					}
				}
			}
			*/
			
			}
			

				
			$scope.gatDayName = function(day)
			{
				var Day = '';
				
				 switch(day)
				 {
					  case 0:
						Day = 'שבת'
						break; 
						
					 case 1:
						Day = 'ראשון'
						break; 
						
					 case 2:
						Day = 'שני'
						break; 
						
					 case 3:
						Day = 'שלישי'
						break; 
						
					 case 4:
						Day = 'רביעי'
						break; 
						
					 case 5:
						Day = 'חמישי'
						break; 
						
					 case 6:
						Day = 'שישי'
						break; 
				 }
				 
				 return Day;
				 
			}
			
			
			$scope.setDays();
			// }, 100);
		//});	
})


.controller('Replay',function($scope,$http,$stateParams,$rootScope) 
{
	$scope.closeReplay = function () {
		$modalInstance.close();
	};
	
	$scope.AddReplayText = function () 
	{
		ReplayArray.push({
				"id": "1",
				"name": angular.element('#ReplayTitle').val(),
				"date": "13.3.15",
				"time": "20:553",
				"img": "img/pubs/review.png",
				"info": angular.element('#ReplayDiscription').val()
			})
			
			console.log("ReplayArray")
			console.log(ReplayArray)
			$modalInstance.close();
	};
	
})


.filter('cutString_ListPage', function () {
    return function (value, wordwise, max, tail) 
	{
        if (!value) return '';
        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;
        value = value.substr(0, max);
        if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace != -1) {
                value = value.substr(0, lastspace);
            }
        }
		return value + (tail || ' …');
    };
})


.filter('cutTitle', function () {
    return function (value, wordwise, max, tail) 
	{
        if (!value) return '';
        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;
        value = value.substr(0, max);
        if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace != -1) {
                value = value.substr(0, lastspace);
            }
        }
		return value;
    };
})




.controller('Days', function($scope,$http,$stateParams,$rootScope) 
{
	$http.get('js/json/days.json').success(function(data)
	{
		$scope.daysArray = data;
	});
})

.controller('LinePage', function($scope,$http,$stateParams,$rootScope,$ionicModal,$localStorage,item,$ionicPopup,OpenFB) 
{
	$scope.$on('$ionicView.enter', function(e) 
  	{
		setTimeout(function(){
			$scope.item = item
			$scope.likeBtnUrl = "likeline2"
			$scope.isTest = false;
			$scope.screenHeight = screen.height-100;

			
  
			$scope.seenmovie = 0;
			$scope.haventseen = 0;
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			$scope.imagePath = $rootScope.Host;
			//$scope.Category = parseInt($rootScope.Category);
			
			//$scope.Category  = 2;
			//$rootScope.Category  = 2;
			//alert($scope.Category)
			
			// For Test
			if($scope.isTest)
			{
				$http.get($rootScope.jsonUrl).success(function(data)
				{
					$scope.Info = data; 
					$scope.ShowInfo();
				});
			}
			else
			{
				if($scope.item == 2 || $scope.item == 4)
				{
					$scope.Info = $rootScope.Lines;
				}
				else
				{
					$scope.Info = $rootScope.Esek;
				}
				/*if($scope.Category == 4)
				{
					/*$scope.jsonUrl = 'js/json/movies.json';
					$http.get($scope.jsonUrl).success(function(data) 
					{
						$scope.Info = data;
					})*/
					/*$scope.Info = $rootScope.Lines;
				}
				else if($scope.Category == 3 || $scope.Category == 2 || $scope.Category == 5)
				{
					$scope.Info = $rootScope.Lines;
				}
				else if($scope.Category == 1 || $scope.Category == 6)
				{
					$scope.Info = $rootScope.Esek;
				}
				else
				{
					
				}*/
			}
			
			if($scope.Info)
			{
				for(i=0;i<$scope.Info.length;i++)
				{
					if($scope.Info[i].index == $stateParams.itemId)
					$scope.ArticleId  = i;
				}
			}
			
			
			$scope.ShowInfo = function()
			{
				$scope.ArticleInfo = $scope.Info[$scope.ArticleId];
				//$scope.jsonUrl = $rootScope.Host+'/getArticles.php?id='+$stateParams.itemId;
				
				LevelOpen = 0;
				
				//alert ($scope.ArticleId);
				
				$scope.LevelOpen = LevelOpen;
				
				$scope.id = $scope.ArticleInfo.index;
				
				$scope.image = $scope.ArticleInfo.Image;
				
				$scope.title = $scope.ArticleInfo.BusinessName;
				
				$scope.about = $scope.ArticleInfo.About;
				
				$scope.address = $scope.ArticleInfo.Address;
				$scope.phone = $scope.ArticleInfo.Phone;
				$scope.hours = $scope.ArticleInfo.Hours;
				$scope.facebook = $scope.ArticleInfo.Facebook;
				$scope.waze = $scope.ArticleInfo.Waze;
				
				$scope.youtube = $scope.ArticleInfo.Youtube;
				$scope.important = $scope.ArticleInfo.importArray;
				$scope.likes = $scope.ArticleInfo.Likes;
				$scope.derug = $scope.ArticleInfo.Derug;
				
				$scope.flikes = $scope.ArticleInfo.FriesndsLikes;
				$scope.shavoa = $scope.ArticleInfo.WeekUpdates;
				if($scope.ArticleInfo.watchedmovie)
				{
					$scope.seenmovie = $scope.ArticleInfo.watchedmovie.length;
					$scope.haventseen = $scope.ArticleInfo.willwatchmovie.length;
				}
				
				//console.log('test1');
				//console.log($rootScope.Esek);
				$scope.Gallery = $scope.ArticleInfo.gallery;
				$rootScope.GalleryInfo  = $scope.Gallery 
				
				$scope.whichItem = $stateParams.itemId;
				$scope.Category = $rootScope.Category;

				
				$scope.catagory_outside = ($scope.ArticleInfo.Catagory_Outside == 1 ? "img/rest/check.png" : "img/rest/ncheck.png");
				$scope.catagory_events = ($scope.ArticleInfo.Catagory_Events == 1 ? "img/rest/check.png" : "img/rest/ncheck.png");
				$scope.catagory_delivery = ($scope.ArticleInfo.Catagory_Delivery == 1 ? "img/rest/check.png" : "img/rest/ncheck.png");
				$scope.catagory_handicap = ($scope.ArticleInfo.Catagory_Handicap == 1 ? "img/rest/check.png" : "img/rest/ncheck.png");
				$scope.catagory_parking = ($scope.ArticleInfo.Catagory_Parking == 1 ? "img/rest/check.png" : "img/rest/ncheck.png");
				$scope.catagory_kosher = ($scope.ArticleInfo.Catagory_Kosher == 1 ? "img/rest/check.png" : "img/rest/ncheck.png");
				$scope.catagory_saturday = ($scope.ArticleInfo.Catagory_Saturday == 1 ? "img/rest/check.png" : "img/rest/ncheck.png");
				$scope.catagory_shows = ($scope.ArticleInfo.Catagory_Shows == 1 ? "img/rest/check.png" : "img/rest/ncheck.png");
				$scope.catagory_wifi = ($scope.ArticleInfo.Catagory_Wifi == 1 ? "img/rest/check.png" : "img/rest/ncheck.png");

				CustomerId = $stateParams.itemId;
				$scope.InfoArray = new Array("משלוחים","אירועים","ישיבה בחוץ","כשר","חנייה","גישה לנכים","WIFI","הופעות","פתוח בשבת")
				$scope.Stars = 0;
				alreadyliked = false;
				
				$scope.increaseLike = function() 
				{
					
					if (!$localStorage.userid)
					{
						var myPopup = $ionicPopup.show({
						title: 'עליך להתחבר כדי לבצע פעולה זו',
						scope: $scope,
						cssClass: 'custom-popup',
						buttons: [
					   {
						text: 'התחברות לפייסבוק',
						type: 'button-calm',
						onTap: function(e) { 
						 window.location.href = "#/app/profile/-1";
						}
					   },
						   {
						text: 'ביטול',
						type: 'button-assertive',
						onTap: function(e) {  
						  //alert (1)
						}
					   },
					   ]
					  });
					}
					else
					{
						if(alreadyliked){ return; }
						$http.get($rootScope.Host+'/update_esek_likes.php?id='+$scope.id+'&user='+$localStorage.userid).success(function(data)
						{
							$scope.likes++;
							$scope.likeBtnUrl = "likeclick"
						});
						alreadyliked = true;				

						$ionicPopup.alert({
						 title: '15 נקודות נוספו לחשבונך',
						buttons: [{
							text: 'אשר',
							type: 'button-positive',
						  }]
					   });
					   
					}
					
					
				

				}
			
			$scope.watchTrailer = function()
			{
				if ($scope.ArticleInfo.MovieTrailer)
				{
					
				var confirmPopup = $ionicPopup.confirm({
				 title: 'לחיצה זו תוציא אותך מהאפליקציה האם לאשר?',
			   });
			   confirmPopup.then(function(res) {
				 if(res) {
				   window.location.href = $scope.ArticleInfo.MovieTrailer;
				 } 
				 });
				}
				else
				{
					$ionicPopup.alert({
					 title: 'לא נוסף טריילר לסרט זה',
					buttons: [{
						text: 'אשר',
						type: 'button-positive',
					  }]
				   });
					
				}
			}	
			
			$scope.WatchedMovieBtn = function()
			{
				//if ($localStorage.WatchedMovieArray[$stateParams.itemId] == 1)
				//{
						data_params = 
						{
							"user" : $localStorage.userid,
							"movie_id" : $stateParams.itemId,
						}
						//console.log(data_params);
						$http.post($rootScope.Host+'/watched_movie.php',data_params).success(function(data)
						{
						 if (data.response.status ==1)
						 {
							 $scope.seenmovie++;
						 }
						 else 
						 {
							$ionicPopup.alert({
							 title: 'כבר סימנת שראית סרט זה',
							buttons: [{
							text: 'אשר',
							type: 'button-positive',
						  }]
						   });
						 }
						});			
				//}
				//else 
				//{
					//$localStorage.WatchedMovieArray[$stateParams.itemId] = 1;
				//}
			}	
			
			$scope.WillWatchMovieBtn = function()
			{
						data_params = 
						{
							"user" : $localStorage.userid,
							"movie_id" : $stateParams.itemId,
						}
						//console.log(data_params);
						$http.post($rootScope.Host+'/will_watch_movie.php',data_params).success(function(data)
						{
						 if (data.response.status ==1)
						 {
							 $scope.haventseen++;
						 }
						 else 
						 {
							 $ionicPopup.alert({
							 title: 'כבר סימנת שהינך מתכונן לראות סרט זה',
							buttons: [{
							text: 'אשר',
							type: 'button-positive',
						  }]							 
						   });
						 }
						});
						
				
			}	
					
			$scope.openInExternalBrowser = function(Url)
			{
				window.open(Url,'_blank','vw=568,vh=768,vx=200,vy=0,buttoncolorbg=#BA8C3C'); 
			};	
			
				$scope.watchedMovieModalBtn = function(type)
				{
					if (type == 1)
					{
					$scope.seenUrl = 'get_seen_movie.php';
						$scope.seenText = 'משתמשים שצפו בסרט ' + $scope.title;
					}
					else 
					{
						$scope.seenUrl = 'get_will_see_movie.php';
						$scope.seenText = 'משתמשים שיצפו בסרט ' + $scope.title;
					}

						$http.post($rootScope.Host+'/'+$scope.seenUrl+'?id='+$stateParams.itemId).success(function(data)
						{
							$scope.SeenMovieArray = data.response;
						});
						
				  $ionicModal.fromTemplateUrl('templates/seen_movie.html', {
					scope: $scope
				  }).then(function(seenMovieShowModal) {
					$scope.seenMovieShowModal = seenMovieShowModal;
					$scope.seenMovieShowModal.show();
				  });
				}
				
				$scope.closeSeenMovieModal = function()
				{
					$scope.seenMovieShowModal.hide();
				}
				
				
				$scope.movieTimes = function () 
		{
					$ionicModal.fromTemplateUrl('templates/movies_times.html', {
					scope: $scope,
					animation: 'slide-in-up',
					focusFirstInput: false
				}).then(function(movieTimesModal) {
					$scope.movieTimesModal = movieTimesModal;
					$scope.movieTimesModal.show();
			}); 	
		}
	

			$scope.closeMovieTimes = function () {
				$scope.movieTimesModal.hide();
			}				
				
				$scope.BirthdaysArray  = $scope.ArticleInfo.birthday;
				$scope.CouponsArray  = $scope.ArticleInfo.coupons;	
				$scope.teamArray  = $scope.ArticleInfo.crew;
					
				$http.get($rootScope.Host+'/getImportantEsek.php?id='+$scope.id).success(function(data)
				{
						$scope.importantArray  = data;
				});
			
			
				$scope.openFacebook = function() 
				{ 
					window.location.href = $scope.facebook;
				}
				
				$scope.openYoutube = function() 
				{
					window.location.href = $scope.youtube;
				}
				
				$scope.openWaze = function() 
				{ 
					window.location.href = "waze://?ll=" + $scope.waze + "&n=T";
				}
			
				$scope.openImport = function ()
				{
					
					 for (i = 0; i < $scope.important.length; i++) 
					 { 
						$scope.importanttext = $scope.important[i];
					 }
					

					$ionicModal.fromTemplateUrl('templates/popups/import_popup.html', {
						scope: $scope,
						animation: 'slide-in-up',
						focusFirstInput: false
					}).then(function(ImportModal) {
						$scope.ImportModal = ImportModal;
						
						if ($scope.importanttext)
						{
						$scope.ImportModal.show();
						
						}
						else 
						{
							 $ionicPopup.alert({
							 title: 'אין כרגע נתונים',
							buttons: [{
							text: 'אשר',
							type: 'button-positive',
						  }]							 
						   });
						}
	
					}); 
				}
				
				$scope.openImportModal = function() {
					$scope.ImportModal.show();
				};
		
				$scope.closeImportModal = function() {
					$scope.ImportModal.hide();
				};
				
				$scope.sendToWaze = function (points)
				{
					window.location.href = "waze://?ll="+points+"&navigate=yes"	
				}
				
				$scope.openChef = function ()
				{
					
				$scope.getRecipieComment = function()
				{
				$http.get($rootScope.Host+'/get_Recipie_Comments.php?esekid='+$scope.id).success(function(data)
				{
					$scope.RecipieComments = data.response;
					//console.log("Replay");
				});				
				}
				
				$scope.getRecipieComment();
					
					$ionicModal.fromTemplateUrl('templates/chef.html', {
						scope: $scope,
						animation: 'slide-in-up',
						focusFirstInput: false
					}).then(function(ChefModal) {
						$scope.ChefModal = ChefModal;
						$scope.ChefModal.show();
					}); 
				}
				
				$scope.openChefModal = function() {
					$scope.ChefModal.show();
				};
		
				$scope.closeChefModal = function() {
					$scope.ChefModal.hide();
				};
				
				
				$scope.ChefComment = 
				{
				"comment" : ""	
				}
				
				$scope.AddCommentChef = function()
				{
					$ionicModal.fromTemplateUrl('templates/popups/add_chef_replay.html', {
						scope: $scope,
						animation: 'slide-in-up',
						focusFirstInput: false
					}).then(function(ChefCommentModal) {
						$scope.ChefCommentModal = ChefCommentModal;
						$scope.ChefCommentModal.show();
					}); 
				}
				
				$scope.closeChefReply = function()
				{
					$scope.ChefCommentModal.hide();
				}
				
				$scope.AddChefComment = function()
				{
					if ($scope.ChefComment.comment)
					{
						comment_data = 
						{
							"user" : $localStorage.userid,
							"id" : $stateParams.itemId,
							"comment" : $scope.ChefComment.comment
						}
						//console.log(comment_data)	
						$http.post($rootScope.Host+'/addRecipeComment.php',comment_data).success(function(data)
						{
							 $ionicPopup.alert({
							 title: 'תגובה נוספה בהצלחה',
							 buttons: [{
							 text: 'אשר',
							 type: 'button-positive',
						     }]							 
						   });
						 $scope.getRecipieComment();
						});
					}
					$scope.ChefComment.comment = '';
					$scope.ChefCommentModal.hide();
				}
				
				
				//Rate Pop up
				$scope.openRate = function ()
				{
					if (!$localStorage.userid)
					{
						var myPopup = $ionicPopup.show({
						title: 'עליך להתחבר כדי לבצע פעולה זו',
						scope: $scope,
						cssClass: 'custom-popup',
						buttons: [
					   {
						text: 'התחברות לפייסבוק',
						type: 'button-calm',
						onTap: function(e) { 
						 window.location.href = "#/app/profile/-1";
						}
					   },
						   {
						text: 'ביטול',
						type: 'button-assertive',
						onTap: function(e) {  
						  //alert (1)
						}
					   },
					   ]
					  });
					}
					else 
					{
						$ionicModal.fromTemplateUrl('templates/popups/rate_popup.html', {
							scope: $scope,
							animation: 'slide-in-up',
							focusFirstInput: false
						}).then(function(RateModal) {
							$scope.RateModal = RateModal;
							$scope.RateModal.show();
						}); 
					}
				}
				
				$scope.OpenRateModal = function() {
					$scope.RateModal.show();
				};
		
				$scope.closeRatePopup = function() 
				{
					$scope.isline = ($scope.item == 1 ? 0 : 1);
					
					if ($scope.Stars) 
					{
						ratedata = 
						{
							"user" : $localStorage.userid,
							"id" : $scope.id,
							"rate" : $scope.Stars,
							"isline" : $scope.isline
						}
							
						$http.post($rootScope.Host+'/rateEsek.php',ratedata).success(function(data)
						{
							$ionicPopup.alert({
							 title: 'דירוג התקבל בהצלחה',
							 subTitle: 'נקודות נוספו לחשבונך 5'
						   });
						});
					}
					
					$scope.RateModal.hide();
				};
				
				$scope.Rate = function(num)
				{
					$scope.Stars = num;
					//alert("Stars : " + $scope.Stars )
				}
				
				$scope.OpenPrvParty = function () 
				{
					$ionicModal.fromTemplateUrl('templates/popups/prv_party_popup.html', {
						scope: $scope,
						animation: 'slide-in-up',
						focusFirstInput: false
					}).then(function(OpenPrvPartyModal) {
						$scope.OpenPrvPartyModal = OpenPrvPartyModal;
						if ($scope.ArticleInfo.prevparties.length == 0)
						{

							 $ionicPopup.alert({
							 title: 'אין כרגע מסיבות קודמות',
							 buttons: [{
						  	 text: 'אשר',
							 type: 'button-positive',
						    }]							 
						   });
						}
						else
						{
							$scope.OpenPrvPartyModal.show();
						}	
						
					}); 	
				}
				
				$scope.closePrvPopup = function() 
				{ 
					$scope.OpenPrvPartyModal.hide();
				}
				
			
				$scope.openBirthdays = function () 
				{
					$ionicModal.fromTemplateUrl('templates/birthday.html', {
						scope: $scope,
						animation: 'slide-in-up',
						focusFirstInput: false
					}).then(function(OpenBirthdaysModal) {
						$scope.OpenBirthdaysModal = OpenBirthdaysModal;
						if ($scope.BirthdaysArray.length == 0)
						{
							$ionicPopup.alert({
							 title: 'אין כרגע ימי הולדת',
							 buttons: [{
							 text: 'אשר',
							 type: 'button-positive',
						     }]							 
						   });
						}
						else
						{
							$scope.OpenBirthdaysModal.show();
						}
						
					}); 	
				}
				
				$scope.closeBirthdaysPopup = function() 
				{ 
					$scope.OpenBirthdaysModal.hide();
				}
			
		
				$scope.openCoupons = function (id) 
				{
					$ionicModal.fromTemplateUrl('templates/coupons.html', {
							scope: $scope,
							animation: 'slide-in-up',
							focusFirstInput: false
						}).then(function(openCouponsModal) {
							$scope.openCouponsModal = openCouponsModal;
							if ($scope.CouponsArray.length == 0)
							{
							
							$ionicPopup.alert({
							 title: 'אין כרגע קופונים',
							  buttons: [{
								text: 'אשר',
								type: 'button-positive',
							  }]
						   });
						   
							}
							else
							{
								$scope.openCouponsModal.show();
							}
							
						}); 	
					}
				
					$scope.closeCouponsPopup = function() { 
					$scope.openCouponsModal.hide();
				}		
				
				
				
				$scope.OpenRestMenu = function()
				{
					$ionicModal.fromTemplateUrl('templates/rest_menu.html', {
							scope: $scope,
							animation: 'slide-in-up',
							focusFirstInput: false
						}).then(function(OpenRestMenuModal) {
							$scope.OpenRestMenuModal = OpenRestMenuModal;
							if ($scope.ArticleInfo.RestImage1)
							{
								$scope.OpenRestMenuModal.show();	
							}
							else
							{
								$ionicPopup.alert({
								 title: 'אין כרגע תפריט למסעדה זו',
							   });
						   
							}
							
					}); 	
				}
				
				
				$scope.closeRestMenuModal = function () 
				{
					$scope.OpenRestMenuModal.hide();
				}
		
		
		
				$scope.Club = function ()
				{
					$ionicModal.fromTemplateUrl('templates/popups/club_popup.html', {
						scope: $scope,
						animation: 'slide-in-up',
						focusFirstInput: false
					}).then(function(ClubModal) {
						$scope.ClubModal = ClubModal;
						$scope.ClubModal.show();
						$scope.showForm = false;
					}); 
				}
				
				$scope.FriendHtml = "";
				
				$scope.addFriendToClub = function()
				{
					$scope.showForm = false;
				}
				
				
				
				$scope.CloseClub = function() {
					$scope.ClubModal.hide();
				};
				
				$scope.signuparams = {
					"firstname" : "",
					"lastname" : "",
					"phone" : "",
					"pub" : ""
				}
				
				
				$scope.SignupClub = function() 
				{
					signupdata = {
						"esekid" : $stateParams.itemId,
						"first" : $scope.signuparams.firstname,
						"last" : $scope.signuparams.lastname,
						"phone" : $scope.signuparams.phone,
						"pub" : $scope.signuparams.pub
					}
					$http.post($rootScope.Host+'/signup_esek_party.php',signupdata).success(function(data)
					{
						$ionicPopup.alert({
						 title: 'נשלח בהצלחה',
					   });
					   $scope.showForm = true;
						//$scope.ClubModal.hide();
					});
				}
				
				$scope.Invite_Show_Popup = function () 
				{
							$ionicModal.fromTemplateUrl('templates/popups/invite_show_popup.html', {
							scope: $scope,
							animation: 'slide-in-up',
							focusFirstInput: false
						}).then(function(inviteShowModal) {
							$scope.inviteShowModal = inviteShowModal;
							$scope.inviteShowModal.show();	
					}); 
					}
				
					$scope.closeInvite_Show_Popup = function()
					{
						$scope.inviteShowModal.hide();	
					}
					
				$scope.invite_Call = function()
				{
					window.location.href = "tel:" + $scope.ArticleInfo.Phone;	
					$scope.inviteShowModal.hide();	
				}
		
				$scope.sendToMail = function(index)
				{
					if (index == 1)
					{
						window.location.href = "mailto:dor@thenest.co.il?subject=פנייה למנהל הליין אפליקצית היציאה"
					}
					else
					{
						//divuh
						window.location.href = "mailto:dor@thenest.co.il?subject=דיווח ליין מאפליקצית היציאה"
					}
				}
				$scope.navigateUrl = function (Path,Num) 
				{
					setTimeout(function(){ window.location.href = Path+String(Num); }, 0);	
				}
				
				$scope.navigateToPub = function (Path,Num) 
				{
					//setTimeout(function(){ window.location.href = Path+String(Num); }, 0);	
					window.location.href = Path+String(Num); 
				}
				
				$scope.navigateGallery = function (Path,Num,Img) 
				{
					$rootScope.SelectedImage  = Img; 
					setTimeout(function(){ window.location.href = Path+String(Num); }, 0);	
				}
				
				
				
				$scope.Call = function (Num) 
				{
					Num = Num.substring(1);
					window.location.href = "tel:+972" + Num;	
				}
				
				$scope.MailTo = function (Num) 
				{
					window.location.href = "mailto:" + Num;	
				}
				
				
				
				$scope.reloadRoute = function () 
				{
					$route.reload();
				}
			
			
				$scope.showInvite = function() 
				{ 
					$ionicModal.fromTemplateUrl('templates/invite_place.html', {
								scope: $scope,
								animation: 'slide-in-up',
								focusFirstInput: false
							}).then(function(showInviteModal) {
								$scope.showInviteModal = showInviteModal;
								$scope.showInviteModal.show();
							}); 	
				}
			
			
				$scope.closeInvite = function () 
				{
					$scope.showInviteModal.hide();
				}
				
				$scope.inviteparams = {
					"name" : "",
					"invMail" : "",
					"invPhone" : "",
					"invDetails" : "",
					"invDate" : "",
					"puppose" : "",
					"location" : "",
					
				}
		

			
				$scope.sendInvite = function() { 
				
				if ($scope.inviteparams.name =="")
				{
						$ionicPopup.alert({
						 title: 'יש למלא שם המזמין',
					   });
				}
				else if ($scope.inviteparams.invMail =="")
				{

						$ionicPopup.alert({
						 title: 'יש למלא מייל',
					   });
				}
				else if ($scope.inviteparams.invPhone =="")
				{
						$ionicPopup.alert({
						 title: 'יש למלא טלפון',
					   });
				}
				else if ($scope.inviteparams.invDate =="")
				{
						$ionicPopup.alert({
						 title: 'יש למלא תאריך הגעה',
					   });
				}
				else if ($scope.inviteparams.puppose =="")
				{
						$ionicPopup.alert({
						 title: 'יש לבחור מטרת הגעה',
					   });
				}	
				else if ($scope.inviteparams.location =="")
				{
						$ionicPopup.alert({
						 title: 'יש לבחור מקום ישיבה',
					   });					
				}					
				else 
				{
				senddata = {
					"esekid" : $scope.id,
					"name" : $scope.inviteparams.name,
					"email" : $scope.inviteparams.invMail,
					"phone" : $scope.inviteparams.invPhone,
					"details" : $scope.inviteparams.invDetails,
					"date" : $scope.inviteparams.invDate,
					"puppose" : $scope.inviteparams.puppose,
					"location" : $scope.inviteparams.location
					
				}
				
				$http.post($rootScope.Host+'/order_reservation.php',senddata).success(function(data)
				{
					$ionicPopup.alert({
					 title: 'נשלח בהצלחה',
				    });
					
					$scope.inviteparams.name = '';
					$scope.inviteparams.invMail = '';
					$scope.inviteparams.invPhone = '';
					$scope.inviteparams.invDetails = '';
					$scope.inviteparams.invDate = '';
					$scope.inviteparams.puppose = '';
					$scope.inviteparams.location = '';
					
				});
			
				$scope.showInviteModal.hide();
			}
			console.log("Inf 9: ")
			}
			
				$scope.OpenTeam = function() 
				{ 
					$ionicModal.fromTemplateUrl('templates/team.html', {
						scope: $scope,
						animation: 'slide-in-up',
						focusFirstInput: false
					}).then(function(showTeamModal) {
						$scope.showTeamModal = showTeamModal;
						if ($scope.teamArray.length == 0)
						{
							$ionicPopup.alert({
							 title: 'אין כרגע אנשי צוות',
							});
						}
						else
						{
							$scope.showTeamModal.show();
						}
						
					}); 	
				}
			
			
				$scope.closeTeam = function () 
				{
					$scope.showTeamModal.hide();
				}
				
				$scope.SetStars = new Array;
				$scope.SetStars[0] = 0;
				$scope.SetStars[1] = 0;
				$scope.SetStars[2] = 0;
	
	
				/*$scope.Rate = function(num,id)
				{	
				alert("Stars : " + num + " : " + id   )							
				if (id =="1")
				{
				$scope.SetStars[0] = num;
				}
				if (id =="2")
				{
					$scope.SetStars[1] = num;
				}
				if (id =="3")
				{
					$scope.SetStars[2] = num;
				}

				}*/
				
				
				
				$scope.rateTeam = function(id)
				{
					
					$scope.rate_team = 
					{
						"desc" : ""
					}
					
					$scope.personImage = $rootScope.Host+$scope.teamArray[id].image;
					$scope.worker_image = $scope.teamArray[id].image;
					$scope.team_name = $scope.teamArray[id].name;
					$scope.team_job = $scope.teamArray[id].position;
					$scope.team_sentense = $scope.teamArray[id].text;
					$scope.team_id = $scope.teamArray[id].index;
					$scope.team_esekindex = $scope.teamArray[id].esek_index;
					$scope.team_isline = $scope.teamArray[id].is_line;
					$scope.showTeamModal.hide();
					$scope.openRateTeam();
				}
				
				
				$scope.openRateTeam = function() 
				{ 
						$ionicModal.fromTemplateUrl('templates/rateteam.html', {
						scope: $scope,
						animation: 'slide-in-up',
						focusFirstInput: false
					}).then(function(rateTeamModal) {
						$scope.rateTeamModal = rateTeamModal;
						$scope.rateTeamModal.show();
				}); 
				
				}
				
				$scope.closeRateTeam = function() 
				{
					$scope.rateTeamModal.hide();
				}
				
				
				$scope.sendRateTeam = function()
				{

					rate_team_params = 
					{
				  "send" : 1,
				  "worker_index" : $scope.team_id,
				  "esek_index" : $scope.team_esekindex,
				  "is_line" : $scope.team_isline,
				  "esek_name" : $scope.title,
				  "worker_name" : $scope.team_name,
				  "worker_position" : $scope.team_job,
				  "worker_image" : $scope.worker_image,
				  "service" : $scope.SetStars[0],
				  "personal" : $scope.SetStars[1],
				  "professional" : $scope.SetStars[2],
				  "own_words" : $scope.rate_team.desc
					}
				
				$http.post($rootScope.Host+'/rate_team.php',rate_team_params).success(function(data)
				{
					$ionicPopup.alert({
					 title: 'נשלח בהצלחה',
					});
				});
				
					
					$scope.rateTeamModal.hide();
				}
				
				
				
				$scope.OpenReview = function() 
				{ 
						$ionicModal.fromTemplateUrl('templates/review.html', {
						scope: $scope,
						animation: 'slide-in-up',
						focusFirstInput: false
					}).then(function(showReviewModal) {
						$scope.showReviewModal = showReviewModal;
						$scope.showReviewModal.show();
				}); 
				console.log("Inf 10: ")	
			}
			
			
			$scope.closeReview = function () 
			{
				$scope.showReviewModal.hide();
			}
		
		
		   $scope.reviewparams  = {
			  "outgoing" : "פאב",
			  "drink" : "וודקה",
			  "music" : "רוק",
			  "week" : "1",
			  "people" : "1",
			  "exposure" : "פייסבוק",
			  "food" : "מזון מהיר",
			  "desc" : ""
		   }
		
			$scope.sendReview = function() 
			{ 
				opiniondata = {
				  "esekid" : $stateParams.itemId,
				  "outgoing" : $scope.reviewparams.outgoing,
				  "drink" : $scope.reviewparams.drink,
				  "music" : $scope.reviewparams.music,
				  "week" : $scope.reviewparams.week,
				  "people" : $scope.reviewparams.people,
				  "exposure" : $scope.reviewparams.exposure,
				  "food" : $scope.reviewparams.food,
				  "desc" : $scope.reviewparams.desc
				}
				
				$http.post($rootScope.Host+'/send_opinion.php',opiniondata).success(function(data)
				{
					$ionicPopup.alert({
					 title: 'נשלח בהצלחה',
					});
					$scope.showReviewModal.hide();
				});
			}
			
			$scope.AddReplay = function () 
			{	

				if (!$localStorage.userid)
				{
					var myPopup = $ionicPopup.show({
					title: 'עליך להתחבר כדי לבצע פעולה זו',
					scope: $scope,
					cssClass: 'custom-popup',
					buttons: [
				   {
					text: 'התחברות לפייסבוק',
					type: 'button-calm',
					onTap: function(e) { 
					 window.location.href = "#/app/profile/-1";
					}
				   },
					   {
					text: 'ביטול',
					type: 'button-assertive',
					onTap: function(e) {  
					  //alert (1)
					}
				   },
				   ]
				  });
				}
				else
				{
					$ionicModal.fromTemplateUrl('templates/popups/add_replay.html', {
							scope: $scope,
							animation: 'slide-in-up',
							focusFirstInput: false
						}).then(function(showAddReply) {
							$scope.showAddReply = showAddReply;
							$scope.showAddReply.show();
					})					
				}
					

			}
		
			$scope.closeReply = function () 
			{
				$scope.showAddReply.hide();
			}
			
			$scope.showComments = function() 
			{
				$http.get($rootScope.Host+'/get_Esek_Comments.php?esekindex='+$scope.id).success(function(data)
				{
					$scope.ReplayArray = data.response;
				});	
			}
			
			$scope.showComments();
			
			
			$scope.comments = {
				"title" : "",
				"comment" : ""
			}
			
			$scope.AddReplayText = function() 
			{
				$scope.isline = ($scope.item == 1 ? 0 : 1);				
				$scope.ReplayArray = {}
	
				if($scope.comments.comment) 
				{
					
					senddata = 
					{
						"id" : $scope.id,
						"comment" : $scope.comments.comment
					}
					//console.log($rootScope.Host+'addEsekComment.php?id='+ $scope.id + '&comment='+$scope.comments.comment)
					$http.get($rootScope.Host+'addEsekComment.php?id='+ $scope.id + '&comment='+$scope.comments.comment+'&user='+$localStorage.userid+'&isline='+$scope.isline).success(function(data)
					{
							$scope.showComments();
							
							$ionicPopup.alert({
							 title: 'תגובה נוספה בהצלחה',
							 subTitle: '10 נקודות נוספו לחשבונך'
						   });
					   
					});
				}
				$scope.showAddReply.hide();
			}
			
			
			
		//	});
			
				$scope.AddFavorite = function () 
				{
					
					$scope.isline = ($scope.item == 1 ? 0 : 1);
				
							$ionicModal.fromTemplateUrl('templates/popups/favorite.html', {
							scope: $scope,
							animation: 'slide-in-up',
							focusFirstInput: false
						}).then(function(showFavorites) {
							
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

					fav_data = 
					{
						"user" : $localStorage.userid,
						"article_id" : $stateParams.itemId,
						"is_line" : $scope.isline

					}
					//console.log(fav_data)					
					$http.post($rootScope.Host+'/add_favorite.php',fav_data).success(function(data)
					{
					});							
							
							$scope.showFavorites = showFavorites;
							$scope.showFavorites.show();
					})
				}
				
			$scope.closeFavoritePopup = function () {
				$scope.showFavorites.hide();
			}

			 $ionicModal.fromTemplateUrl('image-line-modal.html', {
			  scope: $scope,
			  animation: 'slide-in-up'
			}).then(function(LineImageModal) {
			  $scope.LineImageModal = LineImageModal;
			});

	
			$scope.showImageModal = function()
			{
				$scope.LineImageModal.show();
			}
			
			$scope.closeImageLineModal = function()
			{
				$scope.LineImageModal.hide();
			}	
			
			$ionicModal.fromTemplateUrl('image-pub-modal.html', {
			  scope: $scope,
			  animation: 'slide-in-up'
			}).then(function(PubImageModal) {
			  $scope.PubImageModal = PubImageModal;
			});

			$scope.ShowPubImageModal = function()
			{
				$scope.PubImageModal.show();
			}
			
			$scope.closeImagePubModal = function()
			{
				$scope.PubImageModal.hide();
			}
			
			$scope.BigCouponModal = function(index)
			{
			$scope.couponimage = $scope.CouponsArray[index].image;

			$ionicModal.fromTemplateUrl('big-coupon-modal.html', 
			{
					scope: $scope,
					animation: 'slide-in-up',
					focusFirstInput: false
				}).then(function(OpenBigCoupon) {
					$scope.OpenBigCoupon = OpenBigCoupon;
					$scope.OpenBigCoupon.show();
				}); 
			}
			
			$scope.closeBigCouponModal = function()
			{
				$scope.OpenBigCoupon.hide();
			}
			
			$scope.ShareBtn = function()
			{
				
			OpenFB.api(
				{
					method: 'POST',
					path: '/me/feed',
					params: {
						message: 'אפליקצית היציאה'
					},
					success: $scope.successHandler,
					error: $scope.errorHandler
				});		
						
			}
			$scope.successHandler = function(status)
			{
				//alert (status);
			}
			$scope.errorHandler = function(error)
			{
				//alert (error);
			}	
			
			
		};
		
			if(!$scope.isTest)
			$scope.ShowInfo();
	 }, 100);
	})		
})


.controller('inviteController', function($scope,$http,$stateParams,$rootScope,$ionicModal) 
{

    $http.get('js/json/pubs.json').success(function(data)
	{
		$scope.infoArray = data;
		$scope.whichItem = $stateParams.itemId;
	//	$scope.BackUrl = checkBackUrl();
		
		$scope.navigateUrl = function (Path,Num) 
		{
			User = angular.element('#invName').val(); 
			//alert(User)
			Message = 'התקבלה הזמנת מקום חדשב מ '+ User;
			//alert('התקבלה הזמנת מקום חדשב מ ' + User)
			Send_Value("0509666662",Message);
			setTimeout(function(){ window.location.href = Path+String(Num); }, 0);
		}
	});
	

	

	
})

.controller('BirthdayController', function($scope,$http,$stateParams,$rootScope,$ionicModal) 
{
	    $http.get('js/json/pubs.json').success(function(data)
	{
	//	setPageHeight();
		$scope.infoArray = data;
		$scope.whichItem = $stateParams.itemId;
		$scope.BackUrl = checkBackUrl();
		
		
		$scope.navigateUrl = function (Path,Num) 
		{
			window.location.href = Path+String(Num);	
		}
	});
})


.controller('CouponsController', function($scope,$http,$stateParams,$rootScope,$ionicModal) 
{
		$scope.navTitle='<img class="title-image" src="img/main/ylogo.png" />'
		$scope.screenHeight = screen.height-100;

    $http.get('js/json/pubs.json').success(function(data)
	{
	//	console.log(data)
	//	setPageHeight();
		$scope.infoArray = data;
		$scope.whichItem = $stateParams.itemId;
	//	$scope.BackUrl = checkBackUrl();
		
		
		if ($scope.whichItem =="-1")
		{
		$scope.imagePath = $rootScope.Host;
		$http.get($rootScope.Host+'/getGeneralCoupons.php').success(function(data)
		{
			$scope.CouponsArray = data.response;
			console.log($scope.CouponsArray);
		}
		);
		}
		
		
		$scope.navigateUrl = function (Path,Num) 
		{
			window.location.href = Path+String(Num);	
		}
		
		$scope.openCoupon = function () 
		{
					$ionicModal.fromTemplateUrl('templates/popups/coupon_popup.html', {
					scope: $scope,
					animation: 'slide-in-up',
					focusFirstInput: false
				}).then(function(openCouponModal) {
					$scope.openCouponModal = openCouponModal;
					$scope.openCouponModal.show();
			}); 	
	
			$scope.closeCouponModal = function () {
				$scope.openCouponModal.hide();
			}
		}
		
		$scope.BigCoupon = function(index)
		{
			$scope.couponimage = $scope.CouponsArray[index].image;

				$ionicModal.fromTemplateUrl('big-coupon-modal.html', {
					scope: $scope,
					animation: 'slide-in-up',
					focusFirstInput: false
				}).then(function(OpenBigCoupon) {
					$scope.OpenBigCoupon = OpenBigCoupon;
					$scope.OpenBigCoupon.show();
				}); 
		}
		
		$scope.closeBigCouponModal = function()
		{
			$scope.OpenBigCoupon.hide();
		}
		
		
	})
})


.controller('TeamController', function($scope,$http,$stateParams,$rootScope,$ionicModal) 
{
	 $http.get('js/json/team.json').success(function(data)
	{
		setPageHeight();
		$scope.infoArray = data;
		$scope.whichItem = $stateParams.itemId;
		$scope.BackUrl = checkBackUrl();
		
		$scope.navigateUrl = function (Path,Num) 
		{
			window.location.href = Path+String(Num);	
		}
		
		$scope.openCoupon = function () 
		{
			var PopUpImport = $modal.open({
			templateUrl: 'templates/popups/coupon_popup.html',
			controller: 'CouponPopUp'
		})};
	});
})

.controller('ReviewController', function($scope,$http,$stateParams,$rootScope,$ionicModal,$ionicPopup,$localStorage) 
{
	$scope.navTitle='<img class="title-image" src="img/main/ylogo.png" />'

 $http.get('js/json/team.json').success(function(data)
	{
	//	setPageHeight();
		$scope.infoArray = data;
		$scope.whichItem = $stateParams.itemId;
	//	$scope.BackUrl = checkBackUrl();
		
		$scope.navigateUrl = function (Path,Num) 
		{
			window.location.href = Path+String(Num);	
		}
		
		$scope.openCoupon = function () 
		{
			var PopUpImport = $modal.open({
			templateUrl: 'templates/popups/coupon_popup.html',
			controller: 'CouponPopUp'
		})};
	});
	
	if ($stateParams.itemId == -1)
	{
		$scope.reviewparams  = {
			  "outgoing" : "",
			  "drink" : "",
			  "music" : "",
			  "week" : "",
			  "people" : "",
			  "exposure" : "",
			  "food" : "",
			  "desc" : ""
		}
		//$scope.$on('$ionicView.enter', function(e) {

		 $http.get($rootScope.Host+'/Getusers.php?id='+$localStorage.userid).success(function(data)
		{
			
			$scope.reviewparams.outgoing = data.response.fav_activity;
		    $scope.reviewparams.drink = data.response.fav_drink;
			$scope.reviewparams.music = data.response.fav_music;
			$scope.reviewparams.week = data.response.week_hangout;
			$scope.reviewparams.people = data.response.people_hangout;
			$scope.reviewparams.exposure = data.response.hangout_expousure;
			$scope.reviewparams.food = data.response.fav_food;
			$scope.reviewparams.desc = data.response.opinion_desc;
			
		});
		//});
		$scope.closeReview = function()
		{
			//window.history.back();
			window.location.href = "#/app/profile/-1";
		}
		
		$scope.sendReview = function()
		{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

					opinion_data = {
						"id" : $localStorage.userid,
						"outgoing" : $scope.reviewparams.outgoing,
						"drink" : $scope.reviewparams.drink,
						"music" : $scope.reviewparams.music,
						"week" : $scope.reviewparams.week,
						"people" : $scope.reviewparams.people,
						"exposure" : $scope.reviewparams.exposure,
						"food" : $scope.reviewparams.food,
						"desc" : $scope.reviewparams.desc

					}					
					$http.post($rootScope.Host+'/update_user_opinion.php',opinion_data).success(function(data)
					{
						$ionicPopup.alert({
						 title: 'עודכן בהצלחה',
					   });
					});
		}
	
	}
	

})

.controller('FavoritesPage', function($scope,$http,$stateParams,$rootScope,$ionicModal,$localStorage) 
{
	 $scope.$on('$ionicView.enter', function(e) {
 
		$scope.Category = 0;
		$scope.Rand = Math.floor(Math.random()*2)+1;
		$scope.Rand1 = Math.floor(Math.random()*7)+1;
		$scope.Rand2 = Math.floor(Math.random()*7)+1;	
		$scope.imagePath = $rootScope.Host;
	
			$scope.getFavorites = function()
			{
			$http.get($rootScope.Host+'/favorites.php?user='+$localStorage.userid).success(function(data)
			{
				$scope.infoArray = data.response;
			});				
			}

			$scope.getFavorites();

			$scope.navigateFavorites = function(articleindex,line)
			{
				if (line == 1)
				{
					window.location.href = "#/app/Line/"+articleindex;
				}
				else
				{
					window.location.href = "#/app/Pub/"+articleindex;
				}
			}
			
			$scope.deleteFavorite = function(id)
			{

					$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
							
					delete_favorite = 
					{
						"user" : $localStorage.userid,
						"id" : id
						}	
						//console.log(rating_data);
						$http.post($rootScope.Host+'/delete_favorite.php',delete_favorite).success(function(data)
						{
							
						});	 
					
						$scope.getFavorites();
					
			}
	 });		
})

.controller('RateController', function($scope,$http,$stateParams,$rootScope,$ionicModal,$ionicPopup,$localStorage) 
{
	$scope.checked = 0;
	$scope.Stars = 0;
	$scope.SetStars = new Array;
	$scope.SetStars[0] = 0;
	$scope.SetStars[1] = 0;
	$scope.SetStars[2] = 0;
	$scope.SetStars[3] = 0;
	$scope.nomailimage = "img/rest/ncheck.png";
	$scope.nomailvalue = 0;
	$scope.pubsimage = "img/rest/ncheck.png";
	$scope.pubsvalue = 0;	
	$scope.newsimage = "img/rest/ncheck.png";
	$scope.newsvalue = 0;		
	$scope.bowlingimage = "img/rest/ncheck.png";
	$scope.bowlingvalue = 0;		
	$scope.moviesimage = "img/rest/ncheck.png";
	$scope.moviesvalue = 0;		
	$scope.showsimage = "img/rest/ncheck.png";
	$scope.showsvalue = 0;		

	
	$scope.Rate = function(num,id)
	{								
	if (id =="1")
	{
	$scope.SetStars[0] = num;
	}
	if (id =="2")
	{
		$scope.SetStars[1] = num;
	}
	if (id =="3")
	{
		$scope.SetStars[2] = num;
	}
	if (id =="4")
	{
		$scope.SetStars[3] = num;
	}	
	}
	
	$scope.CloseRate = function()
	{
		window.location.href = "#/app/profile/-1";
	}

	$scope.CheckOption = function(id)
	{
		if (id == 1)
		{
			$scope.nomailimage = ($scope.nomailimage == "img/rest/ncheck.png" ? "img/rest/check.png" : "img/rest/ncheck.png");
			$scope.nomailvalue = ($scope.nomailimage == "img/rest/ncheck.png" ? 0 : 1);
		}
		if (id == 2)
		{
			$scope.pubsimage = ($scope.pubsimage == "img/rest/ncheck.png" ? "img/rest/check.png" : "img/rest/ncheck.png");
			$scope.pubsvalue = ($scope.pubsimage == "img/rest/ncheck.png" ? 0 : 1);
		}
		if (id == 3)
		{
			$scope.newsimage = ($scope.newsimage == "img/rest/ncheck.png" ? "img/rest/check.png" : "img/rest/ncheck.png");
			$scope.newsvalue = ($scope.newsimage == "img/rest/ncheck.png" ? 0 : 1);
		}		
		if (id == 4)
		{
			$scope.bowlingimage = ($scope.bowlingimage == "img/rest/ncheck.png" ? "img/rest/check.png" : "img/rest/ncheck.png");
			$scope.bowlingvalue = ($scope.bowlingimage == "img/rest/ncheck.png" ? 0 : 1);
		}
		if (id == 5)
		{
			$scope.moviesimage = ($scope.moviesimage == "img/rest/ncheck.png" ? "img/rest/check.png" : "img/rest/ncheck.png");
			$scope.moviesvalue = ($scope.moviesimage == "img/rest/ncheck.png" ? 0 : 1);
		}	
		if (id == 6)
		{
			$scope.showsimage = ($scope.showsimage == "img/rest/ncheck.png" ? "img/rest/check.png" : "img/rest/ncheck.png");
			$scope.showsvalue = ($scope.showsimage == "img/rest/ncheck.png" ? 0 : 1);
		}			
	}
	
	
	$scope.sendRate = function()
	{
		/*
		 for (i = 0; i < 4; i++) 
		 { 
			if ($scope.SetStars[i] != 0)
			{
				$scope.checked = 1;
			}

		 }
		 
		 if ($scope.checked == 0)
		 {
			 alert ("יש לדרג חוות דעת")
		 }
		 else 
		 {
		*/
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
							
					rating_data = {
						"id" : $localStorage.userid,
						"design" : $scope.SetStars[0],
						"ux" : $scope.SetStars[1],
						"details" : $scope.SetStars[2],
						"effciency" : $scope.SetStars[3],
						"mails" : $scope.nomailvalue,
						"pubs" : $scope.pubsvalue,
						"news" : $scope.newsvalue,
						"bowling" : $scope.bowlingvalue,
						"movies" : $scope.moviesvalue,
						"shows" : $scope.showsvalue,
						"desc" : $scope.desc

						}	
						//console.log(rating_data);
					$http.post($rootScope.Host+'/update_user_rating.php',rating_data).success(function(data)
					{
						$ionicPopup.alert({
						 title: 'עודכן בהצלחה',
					   });
					});	 

	//	 }
		 
	}	

})




.controller('MessagesController', function($scope,$http,$stateParams,$rootScope,$localStorage) 
{
	
		//get user messages
		$http.get($rootScope.Host+'/get_user_messages.php?user='+$localStorage.userid).success(function(data)
	{
		$scope.messageCount = data.response.length; 
		$scope.messagesArray = data.response;
	})
	
	
	//$scope.username = $rootScope.userArray.name;
	//$scope.messageCount = $rootScope.messageCount;
	$scope.imgUrl = $rootScope.Host;
		
		//$scope.messagesArray = $rootScope.userMessagesArray;
					
		$scope.navigateUrl = function (Path,Num) 
		{
			window.location.href = Path+String(Num);	
		}
		
})

.controller('ReadMessageController', function($scope,$http,$stateParams,$rootScope,$localStorage) 
{
	$scope.imgUrl = $rootScope.Host;
	$scope.id = $stateParams.id;

	//get user messages
	$http.get($rootScope.Host+'/get_user_messages.php?user='+$localStorage.userid).success(function(data)
	{
	//$scope.messageCount = data.response.length; 
	$scope.messagesArray = data.response;
		
	$scope.title = $scope.messagesArray[$scope.id].title;
	$scope.description = $scope.messagesArray[$scope.id].desc;
	$scope.image = $scope.messagesArray[$scope.id].image;
	$scope.date = $scope.messagesArray[$scope.id].date;
	$scope.hour = $scope.messagesArray[$scope.id].hour;
	
	
	})

	

	
		$scope.navigateUrl = function (Path,Num) 
		{
			window.location.href = Path+String(Num);	
		}
		
})




.controller('Points', function($scope,$http,$stateParams,$rootScope) 
{
	$scope.$on('$ionicView.enter', function(e) 
  	{
		$scope.navTitle='<img class="title-image" src="img/main/ylogo.png" />'

		$scope.whichItem = $stateParams.itemId;

		$scope.navigateUrl = function (Path,Num) 
		{
			window.location.href = Path+String(Num);	
		}
		
		if ($scope.whichItem == -1)
		{
			 $http.get($rootScope.Host+'/get_users_score.php').success(function(data)
			{
				$scope.scores = data.response;
			});
		}
	})
})

.controller('restMenu', function($scope,$http,$stateParams,$rootScope) 
{
    $http.get('js/json/team.json').success(function(data)
	{
	//	setPageHeight();
		$scope.infoArray = data;
		$scope.whichItem = $stateParams.itemId;
		$scope.MenuUrl = "img/pubs/natbag/menu.jpg"
	//	$scope.BackUrl = checkBackUrl();
		
		$scope.navigateUrl = function (Path,Num) 
		{
			window.location.href = Path+String(Num);	
		}
	});
 
})


.controller('Profile', function($scope,$http,$stateParams,$rootScope,$ionicPopup,$localStorage,OpenFB) 
{
	$scope.navTitle='<img class="title-image" src="img/main/ylogo.png" />'

  //  $http.get('js/json/team.json').success(function(data)
  // {
	  
	  
	 $scope.profileImage = 'img/profile/profile_img.jpg';
	 
	 $scope.getUserProfile = function()
	 {
		 $http.get($rootScope.Host+'/Getusers.php?id='+$localStorage.userid).success(function(data)
		{
		
			$scope.fullname = data.response.name;
			$scope.birth = data.response.birth;
			$scope.sex = data.response.sex;
			$scope.address = data.response.address;
			$scope.email = data.response.email;
			if (data.response.image)
			{
				$scope.profileImage = data.response.image;
			}	
		});		 
	 }
	
	if ($localStorage.userid)
	{
		$scope.getUserProfile();
	}
	

	
	$scope.FaceBookLogin = function()
	{
		
	//public_profile,email,user_birthday
		if ($localStorage.userid)
		{
				facebooklogout();
				//OpenFB.revokePermissions();

				$localStorage.userid = '';
				$localStorage.name = '';
				$localStorage.email = '';
				$localStorage.image = '';
				$scope.profileImage = 'img/profile/profile_img.jpg';
				$scope.getUserProfile();
				
		}
		else 
		{
			//public_profile,user_birthday,publish_stream
			OpenFB.login('email,public_profile,user_birthday,publish_stream,publish_actions').then(function () 
			{
				$scope.getId();
			},
			function () 
			{
				alert('OpenFB login failed');
			});		
		
		}	
	
	}
	
	$scope.getId = function ()
	 {
		 OpenFB.get('/me').success(function (user) 
		 {
			    console.log(user);
            	$scope.user = user;
				//alert(user.name + " : " + user.id);
				$rootScope.isConnected = 1;
				$scope.userimage = 'https://graph.facebook.com/'+user.id+'/picture?type=large';
				$scope.gender = (user.gender == "male" ? "זכר" : "נקבה");
				
					user_params = {
						"id" : user.id,
						"name" : user.name,
						//"birth" : $scope.birth,
						"sex" : $scope.gender,
						//"city" : $scope.address,
						"email" : user.email,
						"image" : $scope.userimage

					}					
					$http.post($rootScope.Host+'/add_new_user.php',user_params).success(function(data)
					{
					 if (data.response.status == 1)
					 {
						$localStorage.userid = data.response.userid;
						$localStorage.name = user.name;
						$localStorage.email = user.email;
						$localStorage.image = $scope.userimage;
						$scope.getUserProfile();
					 }
					 
					});				
				
				
				
         });
	 }
	 
		function facebooklogout()
		{
			OpenFB.logout(
			  function() {
				  //alert ("logout")
			  },
			  errorHandler);			
		}
		
		$scope.UpdateUser = function()
		{
			if ($localStorage.userid)
			{
				$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

				//if (img changed)
					
				
					user_data = {
						"id" : $localStorage.userid,
						"name" : $scope.fullname,
						"birth" : $scope.birth,
						"sex" : $scope.sex,
						"city" : $scope.address,
						"email" : $scope.email

					}					
					$http.post($rootScope.Host+'/updateUsers.php',user_data).success(function(data)
					{
						$ionicPopup.alert({
						 title: 'עודכן בהצלחה',
					   });
					});				
			}
			else
			{
				
			}	

		
		
		}
	//	setPageHeight();

		$scope.whichItem = $stateParams.itemId;
		$scope.MenuUrl = "img/pubs/natbag/menu.jpg"
		$scope.BackUrl = "#/Main/";
		LevelOpen = 3;
	
		$scope.navigateUrl = function (Path,Num) 
		{
			window.location.href = Path+String(Num);	
		}

	function share()
	{
	   openFB.api({
				method: 'POST',
				path: '/me/feed',
				params: {
					message: 'test'
				},
				success: function() {
					alert('the item was posted on Facebook');
				},
				error: errorHandler});
	
	}

		
	
		function errorHandler(error) 
		{
			alert(error.message);
		}
		function successHandler(success)
		{
			alert(success.message);
		}
	//});
	


})

.controller('News', function($scope,$http,$stateParams,$rootScope) 
{
		$scope.navTitle='<img class="title-image" src="img/main/ylogo.png" />'

	    $http.get($rootScope.Host+'/Getnews.php').success(function(data)
	{
		$scope.infoArray = data.response;
		//alert (data.response);
		$scope.imgUrl = $rootScope.Host;
	//	setPageHeight();
	//	$scope.infoArray = $scope.ArticleInfo;
		$scope.whichItem = $stateParams.itemId;
	//	$scope.BackUrl = checkBackUrl();
	//	$scope.InfoArray = new Array("משלוחים","אירועים","ישיבה בחוץ","כשר","חנייה","גישה לנכים","WIFI","הופעות","פתוח בשבת")
		
		$scope.Rand = Math.floor(Math.random()*2)+1;
		$scope.Rand1 = Math.floor(Math.random()*7)+1;
		$scope.Rand2 = Math.floor(Math.random()*7)+1;
		
		$scope.navigateUrl = function (Path,Num) 
		{
			window.location.href = Path+Num;
		}
	});
   
})


.controller('News_page', function($scope,$http,$stateParams,$rootScope,$ionicModal,$ionicPopup,$localStorage,OpenFB) 
{
	$scope.navTitle='<img class="title-image" src="img/main/ylogo.png" />'
	$scope.screenHeight = screen.height-100;
	
	    $http.get($rootScope.Host+'/Getnews.php?id='+$stateParams.itemId).success(function(data)
	{
		$scope.ArticleInfo = data.response;
		$scope.title = $scope.ArticleInfo.title;
		$scope.description = $scope.ArticleInfo.description;
		$scope.imgurl = $rootScope.Host;
		$scope.img2 = $scope.ArticleInfo.img2;
		$scope.img3 = $scope.ArticleInfo.img3;
	//	setPageHeight();
	//	$scope.infoArray = $scope.ArticleInfo;
		$scope.whichItem = $stateParams.itemId;
		//$scope.BackUrl = checkBackUrl();
	});		
		$scope.comments = {
			"comment" : ""
		}
		$scope.Rand = Math.floor(Math.random()*2)+1;
		$scope.Rand1 = Math.floor(Math.random()*7)+1;
		$scope.Rand2 = Math.floor(Math.random()*7)+1;
		
		$scope.navigateUrl = function (Path,Num) 
		{
			window.location.href = Path+String(Num);	
		}
/*		
		$scope.AddReplay = function () 
		{
			var PopUp = $modal.open({
			templateUrl: 'templates/popups/add_replay.html',
			controller: 'Replay'
		})};
		*/
		
		
		
		$scope.getNewsComments = function() {
		$http.get($rootScope.Host+'/getNewsComments.php?catid='+$stateParams.itemId).success(function(data)
		{
			$scope.ReplayArray = data.response;
			
		});
		}
		
		$scope.getNewsComments();

	
		$scope.AddReplay = function () 
		{

			if (!$localStorage.userid)
			{
				var myPopup = $ionicPopup.show({
				title: 'עליך להתחבר כדי לבצע פעולה זו',
				scope: $scope,
				cssClass: 'custom-popup',
				buttons: [
			   {
				text: 'התחברות לפייסבוק',
				type: 'button-calm',
				onTap: function(e) { 
				 window.location.href = "#/app/profile/-1";
				}
			   },
				   {
				text: 'ביטול',
				type: 'button-assertive',
				onTap: function(e) {  
				  //alert (1)
				}
			   },
			   ]
			  });
			}
			else 
			{
					$ionicModal.fromTemplateUrl('templates/popups/add_replay.html', {
					scope: $scope,
					animation: 'slide-in-up',
					focusFirstInput: false
				}).then(function(addCommentModal) {
					$scope.addCommentModal = addCommentModal;
					$scope.addCommentModal.show();
				}); 				
			}
				
	
		}
		
		$scope.closeReply = function()
		{
			$scope.addCommentModal.hide();
		}
		
			   
		
		$scope.AddReplayText = function()
		{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

			if ($scope.comments.comment)
			{
					comment_data = {
						"user" : $localStorage.userid,
						"id" : $stateParams.itemId,
						"comment" : $scope.comments.comment
					}
					$http.post($rootScope.Host+'/addNewsComment.php',comment_data).success(function(data)
					{
						$ionicPopup.alert({
						 title: 'תגובה נוספה בהצלחה',
						 subTitle: '10 נקודות נוספו לחשבונך'
					   });
					   
					});	
					
						$scope.comments.comment = '';
						$scope.addCommentModal.hide();
						$scope.getNewsComments();
						
			}		
		}
	
	
	$ionicModal.fromTemplateUrl('image-modal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(NewsImageModal) {
      $scope.NewsImageModal = NewsImageModal;
    });
	
	
	$scope.ImageModal = function()
	{
		$scope.NewsImageModal.show();
	}
	$scope.closeImageModal = function()
	{
		$scope.NewsImageModal.hide();
	}
	
	$scope.ShareBtn = function()
	{
	
	OpenFB.api(
		{
			method: 'POST',
			path: '/me/feed',
			params: {
				message: 'אפליקצית היציאה'
			},
			success: $scope.successHandler,
			error: $scope.errorHandler
		});		
				
	}
	$scope.successHandler = function(e)
	{
		alert (e);
	}
	$scope.errorHandler = function(e)
	{
		alert (e);
	}	
})

.controller('ReplayController', function($scope,$http,$stateParams,$rootScope) 
{
		$http.get('js/json/replay.json',{dataType:"json",contentType: 'application/json; charset=utf-8'}).success(function(data)
	{
		//	setPageHeight();
			$scope.ReplayArray = data;
		//	$scope.BackUrl = checkBackUrl();
			
			for(var i=0;i<ReplayArray.length;i++)
			$scope.ReplayArray.push(ReplayArray[i]);
			
			$scope.whichItem = $routeParams.itemId;
		});

})
.controller('PubController', function($scope,$http,$stateParams,$rootScope,$ionicModal) 
{
	$scope.$on('$ionicView.enter', function(e) 
  	{
	$http.get('js/json/pubs.json').success(function(data)
	{
		
		LevelOpen = 1;
		$scope.LevelOpen = LevelOpen;
		$scope.infoArray = data;
		$scope.whichItem = $stateParams.itemId;
		CustomerId = $stateParams.itemId;
		$scope.Category = $rootScope.Category;
		
		$scope.InfoArray = new Array("משלוחים","אירועים","ישיבה בחוץ","כשר","חנייה","גישה לנכים","WIFI","הופעות","פתוח בשבת")
		
		
		
		if($scope.Category == 1 || $scope.Category == 4 || $scope.Category == 6)
		{
			$scope.NextPage = "#/List/"
			$scope.Category = $rootScope.Category;
		}
		else
		{
			$scope.NextPage = "#/Line/"
			$scope.rCategory = $stateParams.itemId;
			$scope.Category = $scope.whichItem;
		}
		
		
		$scope.openImport = function () 
		{
			var PopUpImport = $modal.open({
			templateUrl: 'templates/popups/import_popup.html',
		})};
		

		
		$scope.ShowBirthdays = function () 
		{
					$ionicModal.fromTemplateUrl('templates/birthday.html', {
					scope: $scope,
					animation: 'slide-in-up',
					focusFirstInput: false
				}).then(function(openBirthdaysModal) {
					$scope.openBirthdaysModal = openBirthdaysModal;
					$scope.openBirthdaysModal.show();
			}); 	
		}
	
	
		$scope.closeBirthdaysPopup = function () 
		{
				$scope.openBirthdaysModal.hide();
		}
	
		$scope.ShowCoupons = function () 
		{
					$ionicModal.fromTemplateUrl('templates/coupons.html', {
					scope: $scope,
					animation: 'slide-in-up',
					focusFirstInput: false
				}).then(function(openCouponsModal) {
					$scope.openCouponsModal = openCouponsModal;
					$scope.openCouponsModal.show();
			}); 	
		}
		
	
		$scope.closeCouponsPopup = function () 
		{
			$scope.openCouponsModal.hide();
		}
		
		
		
		$scope.AddReplayModal = function () 
		{
					$ionicModal.fromTemplateUrl('templates/popups/add_replay.html', {
					scope: $scope,
					animation: 'slide-in-up',
					focusFirstInput: false
				}).then(function(openReplyModal) {
					$scope.openReplyModal = openReplyModal;
					$scope.openReplyModal.show();
			}); 	
		}
	
	
			$scope.closeReply = function () {
				$scope.openReplyModal.hide();
			}
			

		
			$scope.openTeamModal = function () 
		{
					$ionicModal.fromTemplateUrl('templates/team.html', {
					scope: $scope,
					animation: 'slide-in-up',
					focusFirstInput: false
				}).then(function(openTeam) {
					$scope.openTeam = openTeam;
					$scope.openTeam.show();
			}); 	
		}
	
	
			$scope.closeTeam = function () {
				$scope.openTeam.hide();
			}		
		
				$scope.ReviewOpen = function () 
		{
					$ionicModal.fromTemplateUrl('templates/review.html', {
					scope: $scope,
					animation: 'slide-in-up',
					focusFirstInput: false
				}).then(function(openReview) {
					$scope.openReview = openReview;
					$scope.openReview.show();
			}); 	
		}
	
	
			$scope.closeReview = function () {
				$scope.openReview.hide();
			}	
			
			
		$scope.openRate = function () 
		{
			var RatePopUp = $modal.open({
			templateUrl: 'templates/popups/rate_popup.html',
			controller: 'ImportPopUp'
		})};
		
		$scope.openWeek = function () 
		{
			var RatePopUp = $modal.open({
			templateUrl: 'templates/popups/week.html',
			controller: 'WeekPopUp'
		})};
		
		$scope.Invite_Show_Popup = function () 
		{
			var PopUp = $modal.open({
			templateUrl: 'templates/popups/invite_show_popup.html',
			controller: 'Invite_Show_Popup'
		})};
		
		
		$scope.OpenSeasonTicket = function () 
		{
			var RatePopUp = $modal.open({
			templateUrl: 'templates/popups/season_ticket.html',
			controller: 'OpenSeasonTicket'
		})};
		
		$scope.OpenPrvParty = function () 
		{
			var PopUp = $modal.open({
			templateUrl: 'templates/popups/prv_party_popup.html',
			controller: 'PrvParty'
		})};
		
		/*
		$scope.AddFavorite = function () 
		{
			var Favorite = $modal.open({
			templateUrl: 'templates/popups/favorite.html',
			controller: 'Favorite'
		})};
		*/
		$scope.AddReplay = function () 
		{
			var PopUp = $modal.open({
			templateUrl: 'templates/popups/add_replay.html',
			controller: 'Replay'
		})};
		

	
		$scope.navigateUrl = function (Path,Num) 
		{
			setTimeout(function(){ window.location.href = Path+String(Num); }, 0);	
		}
		
		$scope.reloadRoute = function () 
		{
			$route.reload();
		};
		
		$scope.Call = function (Num) 
		{
			Num = Num.substring(1);
			window.location.href = "tel:+972" + Num;	
		}
		

		$scope.MailTo = function (Num) 
		{
			window.location.href = "mailto:" + Num;	
		}
	
	 })
   })
})



.controller('Movies', function($scope,$http,$stateParams,$rootScope) 
{
   /* $http.get('js/json/movies.json').success(function(data)
	{
		setPageHeight();
		$scope.infoArray = data;
		$scope.whichItem = $stateParams.itemId;
	//	$scope.BackUrl = checkBackUrl();
		
		$scope.navigateUrl = function (Path,Num) 
		{
			window.location.href = Path+String(Num);	
		}
	});*/
})

.controller('GalleryController', function($scope,$http,$stateParams,$rootScope,$ionicSlideBoxDelegate,$ionicSideMenuDelegate) 
{
	
	$scope.$on('$ionicView.enter', function()
	{
      $ionicSideMenuDelegate.canDragContent(false);
	  $ionicSlideBoxDelegate.slide($rootScope.SelectedImage);
    });
	
  	$scope.$on('$ionicView.leave', function()
  	{
      $ionicSideMenuDelegate.canDragContent(true);
    });
	
	

	$scope.navTitle='<img class="title-image" src="img/main/ylogo.png" />'
	$scope.Gallery = $rootScope.GalleryInfo
	$scope.imagePath = $rootScope.Host;
	$scope.galleryWidth = String((125*$scope.Gallery.length)) + 'px';
		
	$scope.CurrentGalleryImage = $scope.Gallery[$rootScope.SelectedImage].image;
	
	
	$scope.Gal_Btn = function(num) 
	{
       	$scope.CurrentGalleryImage = $scope.Gallery[num].image;
		$ionicSlideBoxDelegate.slide(num);
		console.log("CurrentGalleryImage : " + $scope.CurrentGalleryImage)
    };
})

.controller('ContactCtrl', function($scope,$http,$stateParams,$rootScope) 
{
	$scope.navTitle='<img class="title-image" src="img/main/ylogo.png" />'
})

.controller('GeneralCtrl', function($scope,$http,$stateParams,$rootScope,$ionicModal) 
{
	$scope.$on('$ionicView.enter', function()
	{
	$scope.navTitle='<img class="title-image" src="img/main/ylogo.png" />';
	$scope.imagePath = $rootScope.Host;

	$scope.AboutText = $rootScope.GeneralData[1].text;
	$scope.TermsText = $rootScope.GeneralData[0].text;
	$scope.Crew = $rootScope.CrewData;
	$scope.Faq = $rootScope.QuestionsData;
	
	$scope.ShowAboutModal = function()
	{
		$ionicModal.fromTemplateUrl('templates/popups/about.html', 
		{
			scope: $scope,
			animation: 'slide-in-up',
			focusFirstInput: false
		}).then(function(AboutUsModal) {
			$scope.AboutUsModal = AboutUsModal;
			$scope.AboutUsModal.show();
		}); 
	}
	
	$scope.closeAboutModal = function()
	{
		$scope.AboutUsModal.hide();
	}
	
	$scope.ShowFaqModal = function()
	{
		$ionicModal.fromTemplateUrl('templates/popups/faq.html', 
		{
			scope: $scope,
			animation: 'slide-in-up',
			focusFirstInput: false
		}).then(function(FaqModal) {
			$scope.FaqModal = FaqModal;
			$scope.FaqModal.show();
		}); 		
	}
	
	$scope.closeFaqModal = function()
	{
		$scope.FaqModal.hide();
	}
	
	$scope.ShowCrewModal = function()
	{
		$ionicModal.fromTemplateUrl('templates/popups/crew.html', 
		{
			scope: $scope,
			animation: 'slide-in-up',
			focusFirstInput: false
		}).then(function(CrewModal) {
			$scope.CrewModal = CrewModal;
			$scope.CrewModal.show();
		}); 		
	}
	
	$scope.closeCrewModal = function()
	{
		$scope.CrewModal.hide();
	}
	
	$scope.ShowTermsModal = function()
	{
		$ionicModal.fromTemplateUrl('templates/popups/terms.html', 
		{
		scope: $scope,
		animation: 'slide-in-up',
		focusFirstInput: false
	}).then(function(TermsModal) {
		$scope.TermsModal = TermsModal;
		$scope.TermsModal.show();
		}); 		
	}
	
	$scope.closeTermsModal = function()
	{
		$scope.TermsModal.hide();
	}
	
	$scope.ShowPointsModal = function()
	{
		$ionicModal.fromTemplateUrl('templates/popups/points.html', 
		{
			scope: $scope,
			animation: 'slide-in-up',
			focusFirstInput: false
		}).then(function(PointsModal) {
			$scope.PointsModal = PointsModal;
			$scope.PointsModal.show();
		}); 		
	}
	
	$scope.closePointsModal = function()
	{
		$scope.PointsModal.hide();
	}
	});
})


.controller('NearByCtrl', function($scope,$http,$stateParams,$rootScope,$ionicModal,$localStorage) 
{
	 $scope.$on('$ionicView.enter', function(e) {
 
		$scope.Category = 0;
		$scope.Rand = Math.floor(Math.random()*2)+1;
		$scope.Rand1 = Math.floor(Math.random()*7)+1;
		$scope.Rand2 = Math.floor(Math.random()*7)+1;	
		$scope.imagePath = $rootScope.Host;
		$scope.Eesk = $rootScope.Esek;
		$scope.Lines = $rootScope.Lines;
		
		$scope.EsekAndLines = $scope.Eesk.concat($scope.Lines);
		//console.log($scope.EsekAndLines)
		
			$scope.navigateNearby = function(articleId,id)
			{
				if (articleId)
				{
					window.location.href = "#/app/Line/"+id;
				}
				else
				{
					window.location.href = "#/app/Pub/"+id;
				}
			}

	 });		
})



.filter('BirthDate', function() 
{
  return function(input) {
  splitdate = input.split("-"); 
  if (input)
  {
	  return splitdate[2]+'/'+splitdate[1]+'/'+splitdate[0];
  }
  else
  {
	  return '';
  }
  
  };
})

/*
.filter('LineDate', function() 
{
  return function(input) {
  console.log(input)
  };
})
*/


.filter('InfoPopUp', function() 
{
  return function(input, total) {
    total = parseInt(total);
    for (var i=0; i<total; i++)
      input.push(i);
    return input;
  };
})

.filter('range', function() 
{
  return function(input, total) 
  {
    total = parseInt(total);
    for (var i=0; i<total; i++)
      input.push(i);
    return input;
  };
});


function shuffle(o)
{ //v1.0
   	for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
}